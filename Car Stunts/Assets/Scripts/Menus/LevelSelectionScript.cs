﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionScript : MonoBehaviour
{
    public GameObject levelPrefab;
    public List<GameObject> spawnedLevels;
    public Transform levelsParent;
    public Sprite[] levelsSprite;
    public Sprite lockSprite;
    public Button unlockallButton, backButton;

    private void OnDisable()
    {
        foreach (GameObject card in spawnedLevels)
            Destroy(card);

        spawnedLevels.Clear();
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        if (MainMenuController.instance)
            MainMenuController.instance.camInitPos.z = 12f;
        StartCoroutine(InitLevelsSpirte());
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    IEnumerator InitLevelsSpirte()
    {
        unlockallButton.onClick.RemoveAllListeners();
        unlockallButton.onClick.AddListener(UnlockAll);

        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(Back);

        spawnedLevels = new List<GameObject>();
        GameConstants.unlockItem(GameConstants.levelLockingKey + "" + 0);
        for (int i = 0;i<levelsSprite.Length;i++)
        {
            GameObject levelcard = Instantiate(levelPrefab, levelsParent);
            spawnedLevels.Add(levelcard);
            LevelCard levelcardScript = levelcard.GetComponent<LevelCard>();
            levelcardScript.unLockImage = levelsSprite[i];
            levelcardScript.lockImage = lockSprite;
            levelcardScript.LevelIndex = i;
            if (GameConstants.isItemUnlocked(GameConstants.levelLockingKey + "" + i))
                levelcardScript.SetUnlocked();
            else
                levelcardScript.SetLocked();

            yield return new WaitForSeconds(0.05f);

        }
    }

    public void UnlockAll()
    {

    }


    public void Back()
    {
        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.CarSelection);
        Destroy(this.gameObject);

    }

}
