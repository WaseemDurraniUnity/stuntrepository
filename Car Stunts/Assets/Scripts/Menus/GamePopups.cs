﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GamePopups : MonoBehaviour
{
    public enum PopupState
    {
        Discription  , Pause , GameFailed , GameComplete
    }

    public static PopupState curState;
    public Image img_BG;
    public Text txt_Title, txt_Discription;
    public Button btn_Start, btn_Home, btn_Restart, btn_Next,btn_Resume;
    public GameObject starsObject;
    public GameObject[] stars;

    public static string discription;
    public static UnityAction event_onCompleteDiscp;

    private Color color_BG;
    // Start is called before the first frame update
    void Start()
    {
        InitPopup();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void InitPopup()
    {
        switch (curState)
        {
            case PopupState.Discription:
                color_BG = img_BG.color;
                color_BG.a = 0.5f;
                img_BG.color = color_BG;

                txt_Title.text = "LEVEL "+(GameConstants.GetCurrentLevelToPlay() + 1);
                txt_Discription.gameObject.SetActive(true);
                txt_Discription.text = discription;
                btn_Start.gameObject.SetActive(true);
                btn_Home.gameObject.SetActive(false);
                btn_Next.gameObject.SetActive(false);
                btn_Resume.gameObject.SetActive(false);
                btn_Restart.gameObject.SetActive(false);

                break;


            case PopupState.Pause:
                color_BG = img_BG.color;
                color_BG.a = 1f;
                img_BG.color = color_BG;

                txt_Title.text = "GAME PAUSED!";
                txt_Discription.gameObject.SetActive(false);
                starsObject.SetActive(true);

                btn_Start.gameObject.SetActive(false);
                btn_Home.gameObject.SetActive(true);
                btn_Next.gameObject.SetActive(false);
                btn_Resume.gameObject.SetActive(true);
                btn_Restart.gameObject.SetActive(true);

                Invoke("StopGame",1.5f);

                break;


            case PopupState.GameFailed:
                color_BG = img_BG.color;
                color_BG.a = 1f;
                img_BG.color = color_BG;
                txt_Title.text = "FAILED!";
                txt_Discription.gameObject.SetActive(false);
                starsObject.SetActive(true);

                btn_Start.gameObject.SetActive(false);
                btn_Home.gameObject.SetActive(true);
                btn_Next.gameObject.SetActive(false);
                btn_Resume.gameObject.SetActive(false);
                btn_Restart.gameObject.SetActive(true);

                break;

            case PopupState.GameComplete:
                color_BG = img_BG.color;
                color_BG.a = 1f;
                img_BG.color = color_BG;

                txt_Title.text = "Completed!";
                txt_Discription.gameObject.SetActive(false);
                starsObject.SetActive(true);
                SetStars(GameConstants.GetStars());

                btn_Start.gameObject.SetActive(false);
                btn_Home.gameObject.SetActive(true);
                btn_Next.gameObject.SetActive(true);
                btn_Resume.gameObject.SetActive(false);
                btn_Restart.gameObject.SetActive(true);

                break;
        }

        // Assign functions
        btn_Start.onClick.RemoveAllListeners();
        btn_Start.onClick.AddListener(StartButtonClicked);

        btn_Home.onClick.RemoveAllListeners();
        btn_Home.onClick.AddListener(HomeButtonClicked);

        btn_Next.onClick.RemoveAllListeners();
        btn_Next.onClick.AddListener(NextButtonClicked);

        btn_Restart.onClick.RemoveAllListeners();
        btn_Restart.onClick.AddListener(RestartButtonClicked);

        btn_Resume.onClick.RemoveAllListeners();
        btn_Resume.onClick.AddListener(ResumeButtonClicked);


    }

    void StopGame()
    {
        Time.timeScale = 0;
    }

    void SetStars(int no)
    {
        for(int i = 0; i < no; i++)
        {
            stars[i].SetActive(true);
        }
    }


    public void StartButtonClicked()
    {
        if (event_onCompleteDiscp != null)
            event_onCompleteDiscp.Invoke();

        event_onCompleteDiscp = null;

        Destroy(this.gameObject);

    }


    public void HomeButtonClicked()
    {
        LoadingScript.LeveltoLoad = "garage";
        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.LoadingMenu);

        Destroy(this.gameObject);

    }

    public void RestartButtonClicked()
    {
        LoadingScript.LeveltoLoad = "level 1";
        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.LoadingMenu);

        Destroy(this.gameObject);

    }

    public void NextButtonClicked()
    {
        if (GameConstants.GetCurrentLevelToPlay() < GameManager.instance.levelsPrefabs.Length - 1)
        {
            GameConstants.SetCurrentLevelToPlay(GameConstants.GetCurrentLevelToPlay() + 1);
            LoadingScript.LeveltoLoad = "level 1";

        }
        else
            LoadingScript.LeveltoLoad = "garage";


        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.LoadingMenu);

        Destroy(this.gameObject);

    }

    public void ResumeButtonClicked()
    {
        Time.timeScale = 1;

        Destroy(this.gameObject);

    }
}
