﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSelectionScript : MonoBehaviour
{ 
    public delegate void CarDetails();
    public static CarDetails SetCarDetails;
    public GameObject carScroller;
    public GameObject detailsObj;
    public GameObject carCardPrefab;
    private List<GameObject> spawnedPrefabs = new List<GameObject>();
    public Transform cardSpawnParent;
    private int carCount;
    public Button btn_Next, btn_previous;
    [Header("Car Details")]
    public Text carName;
    public Slider slider_Speed, slider_Power, slider_Brake, slider_Drift;
    public Text txt_priceinCash, txt_priceinGold;
    public Button btn_Drive, btn_priceinCash, btn_priceinGold , btn_Back;
    private CarModel curCarModel;

    private void OnEnable()
    {
        SetCarDetails += UpdateCarDetails;
        MainMenuController.instance.camInitPos.z = 3.99f;
        InitCarSelectionSettings();
    }


    private void OnDisable()
    {
        SetCarDetails -= UpdateCarDetails;
        foreach (GameObject card in spawnedPrefabs)
            Destroy(card);

        spawnedPrefabs.Clear();

    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (curCarModel)
        {
            slider_Speed.value = Mathf.Lerp(slider_Speed.value, curCarModel.Speed,Time.deltaTime*5f);
            slider_Power.value = Mathf.Lerp(slider_Power.value, curCarModel.Power, Time.deltaTime * 5f);
            slider_Brake.value = Mathf.Lerp(slider_Brake.value, curCarModel.Brake, Time.deltaTime * 5f);
            slider_Drift.value = Mathf.Lerp(slider_Drift.value, curCarModel.Drift, Time.deltaTime * 5f);

        }
        //if (canLerp)
        //{

        //    if (Vector3.Distance(contentPanel.anchoredPosition, finalCardPos) > 0)
        //    {
        //        contentPanel.anchoredPosition = Vector2.Lerp(contentPanel.anchoredPosition, finalCardPos
        //                , Time.deltaTime * 5f);
        //    }
        //    else if (Vector3.Distance(contentPanel.anchoredPosition, finalCardPos) <= 0)
        //    {
        //        canLerp = false;
        //    }
        //}
    }


    void InitCarSelectionSettings()
    {
        spawnedPrefabs = new List<GameObject>();
        btn_Next.onClick.RemoveAllListeners();
        btn_Next.onClick.AddListener(NextCar);
        btn_previous.onClick.RemoveAllListeners();
        btn_previous.onClick.AddListener(PreviousCar);
        btn_Drive.onClick.RemoveAllListeners();
        btn_Drive.onClick.AddListener(DriveCar);
        btn_Back.onClick.RemoveAllListeners();
        btn_Back.onClick.AddListener(Back);
        btn_priceinCash.onClick.RemoveAllListeners();
        btn_priceinCash.onClick.AddListener(PurchaseByCash);
        btn_priceinGold.onClick.RemoveAllListeners();
        btn_priceinGold.onClick.AddListener(PurchaseByGold);

        GameConstants.unlockItem(GameConstants.carLockingKey +""+ 0); //  Unlocking First Car

        carCount = MainMenuController.instance.carsPrefabs.Length;
        for(int i = 0; i < carCount; i++)
        {
            GameObject card = Instantiate(carCardPrefab, cardSpawnParent);
            spawnedPrefabs.Add(card);
            CarSelectionCards cardScript = card.GetComponent<CarSelectionCards>();
            cardScript.renderImage.sprite = MainMenuController.instance.carsPrefabs[i].carRender;
            cardScript.carIndex = i;
            if (GameConstants.isItemUnlocked(GameConstants.carLockingKey +"" + i))
                cardScript.SetUnlocked();
            else
                cardScript.SetLocked();
        }

        MainMenuController.instance.SetCar();
    }

    public void NextCar()
    {
        int curIndex = MainMenuController.instance.GetCarIndex();
        int cartoCheck = curIndex < carCount - 1 ? curIndex + 1 : 0;
        MainMenuController.instance.SetNextCar();
        SnapTo(spawnedPrefabs[curIndex].GetComponent<RectTransform>());


    }
    public void PreviousCar()
    {
        int curIndex = MainMenuController.instance.GetCarIndex();
        int cartoCheck = curIndex > 0 ? curIndex - 1 : carCount - 1;
        MainMenuController.instance.SetPreviousCar();
        SnapTo(spawnedPrefabs[curIndex].GetComponent<RectTransform>());


    }

    public void UpdateCarDetails()
    {
        curCarModel = MainMenuController.instance.GetCurCarModel();
        carName.text = curCarModel.carName;
        bool isUnlocked = GameConstants.isItemUnlocked(GameConstants.carLockingKey + MainMenuController.instance.GetCarIndex());
        txt_priceinCash.text = curCarModel.priceinCash.ToString();
        txt_priceinGold.text = curCarModel.priceinGold.ToString();
        btn_Drive.gameObject.SetActive(isUnlocked);
        btn_priceinCash.gameObject.SetActive(!isUnlocked);
        btn_priceinGold.gameObject.SetActive(!isUnlocked);

    }



    public void DriveCar()
    {
        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.LevelSelection);
        Destroy(this.gameObject);

    }


    public void PurchaseByCash()
    {
        if(GameConstants.GetCash() >= MainMenuController.instance.GetCurCarModel().priceinCash)
        {

            AlertMenuScript.title = "You want to Purchase this Car?";
            AlertMenuScript.yesText = "Yes";
            AlertMenuScript.NoText = "No";
            AlertMenuScript.yesEvent = ConfirmPurchaseByCash;
            MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.AlertMenu);
        
        }
        else
        {
            AlertMenuScript.title = "Sorry You Dont Have Enough Cash!";
            AlertMenuScript.yesText = "OK";
            AlertMenuScript.NoText = "Free Cash";
            AlertMenuScript.noEvent = WatchAd;
            MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.AlertMenu);

        }
    }


    public void ConfirmPurchaseByCash()
    {
        GameConstants.DeductCash(MainMenuController.instance.GetCurCarModel().priceinCash);
        GameConstants.unlockItem(GameConstants.carLockingKey + "" + MainMenuController.instance.GetCarIndex());
        HeaderScript.instance.UpdateHeader();
        UpdateCarDetails();
    }




    void WatchAd()
    {
        GameConstants.AddCash(50);
        HeaderScript.instance.UpdateHeader();

    }

    void OpenShop()
    {

    }

    public void PurchaseByGold()
    {
        if (GameConstants.GetGold() >= MainMenuController.instance.GetCurCarModel().priceinGold)
        {

            AlertMenuScript.title = "You want to Purchase this Car?";
            AlertMenuScript.yesText = "Yes";
            AlertMenuScript.NoText = "No";
            AlertMenuScript.yesEvent = ConfirmPurchaseByGold;
            MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.AlertMenu);


        }
        else
        {
            AlertMenuScript.title = "Sorry You Dont Have Enough Gold!";
            AlertMenuScript.yesText = "OK";
            AlertMenuScript.NoText = "Buy Gold";
            AlertMenuScript.noEvent = OpenShop;
            MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.AlertMenu);
        }
    }

    public void ConfirmPurchaseByGold()
    { 
        GameConstants.DeductGold(MainMenuController.instance.GetCurCarModel().priceinGold);
        GameConstants.unlockItem(GameConstants.carLockingKey + "" + MainMenuController.instance.GetCarIndex());
        HeaderScript.instance.UpdateHeader();
        UpdateCarDetails();
    }

    public void Back()
    {
        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.MainMenu);
        Destroy(this.gameObject);

    }


    public void CustomizeCar()
    {
        carScroller.gameObject.SetActive(false);
        detailsObj.SetActive(false);
        btn_Next.gameObject.SetActive(false);
        btn_previous.gameObject.SetActive(false);
        MainMenuController.instance.Customization();
    }


    // Scroll Car
    public ScrollRect scrollRect;
    public RectTransform contentPanel;
    Vector2 finalCardPos;
    private bool canLerp = false;
    public float pos;
    public int siblingIndex;
    public void SnapTo(RectTransform target)
    {


        Canvas.ForceUpdateCanvases();


        siblingIndex = spawnedPrefabs[MainMenuController.instance.GetCarIndex()].transform.GetSiblingIndex();

        pos = (float)siblingIndex / scrollRect.content.transform.childCount;

        //if (pos < 0.4)
        //{
        //    float correction = 1f / scrollRect.content.transform.childCount;
        //    pos -= correction;
        //}

        scrollRect.horizontalNormalizedPosition = pos;


        //Vector2 finalpos = (Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position)
        //    - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
        
        ////contentPanel.anchoredPosition = new Vector2(finalpos.x + 120, finalpos.y);
        //finalCardPos = new Vector2(finalpos.x + 120, finalpos.y);
        canLerp = true;



    

    }


}
