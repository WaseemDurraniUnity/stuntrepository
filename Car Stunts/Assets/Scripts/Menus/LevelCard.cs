﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LevelCard : MonoBehaviour
{
    [HideInInspector]
    public Image img_Level;
    [HideInInspector]
    public Button btn_Level;
    [HideInInspector]
    public Sprite unLockImage;
    [HideInInspector]
    public Sprite lockImage;

    public int LevelIndex;
    // Start is called before the first frame update
    void Start()
    {
        img_Level = GetComponent<Image>();
        btn_Level = GetComponent<Button>();

        btn_Level.onClick.RemoveAllListeners();
        btn_Level.onClick.AddListener(OnTap);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, Time.deltaTime * 10f);
    }

    public void SetLocked()
    {
        img_Level.sprite = lockImage;
    }

    public void SetUnlocked()
    {
        img_Level.sprite = unLockImage;

    }


    public void OnTap()
    {
        if (GameConstants.isItemUnlocked(GameConstants.levelLockingKey + "" + LevelIndex))
        {
            LoadingScript.LeveltoLoad = "level 1";
            GameConstants.SetCurrentLevelToPlay(LevelIndex);
            MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.LoadingMenu);
        }

        Destroy(this.transform.parent.transform.parent.transform.parent.transform.parent.gameObject);

    }
}
