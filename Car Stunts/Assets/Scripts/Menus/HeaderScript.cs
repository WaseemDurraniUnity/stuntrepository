﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderScript : MonoBehaviour
{
    public static HeaderScript instance;

    public Text txt_Cash, txt_Gold;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateHeader();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void UpdateHeader()
    {
        txt_Cash.text = GameConstants.GetCash().ToString();
        txt_Gold.text = GameConstants.GetGold().ToString();

    }
}
