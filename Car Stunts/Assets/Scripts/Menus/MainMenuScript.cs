﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public GameObject[] mainMenuObjects;
    public float delayForActive;
    public Button btn_Play, btn_Rateus, btn_MoreGames, btn_RestorePurchase, btn_LeaderBoard, btn_NoAds, btn_FreeGift;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ActiveObjects());
    }

    IEnumerator ActiveObjects()
    {
        for(int i = 0; i < mainMenuObjects.Length; i++)
        {
            yield return new WaitForSeconds(delayForActive);
            mainMenuObjects[i].SetActive(true);
        }

        InitMainMenu();
    }


    void InitMainMenu()
    {
        btn_Play.onClick.RemoveAllListeners();
        btn_Play.onClick.AddListener(Play);

        btn_Rateus.onClick.RemoveAllListeners();
        btn_Rateus.onClick.AddListener(RateUs);

        btn_MoreGames.onClick.RemoveAllListeners();
        btn_MoreGames.onClick.AddListener(MoreGames);

        btn_RestorePurchase.onClick.RemoveAllListeners();
        btn_RestorePurchase.onClick.AddListener(RestorePurchase);

        btn_FreeGift.onClick.RemoveAllListeners();
        btn_FreeGift.onClick.AddListener(FreeGift);

        btn_LeaderBoard.onClick.RemoveAllListeners();
        btn_LeaderBoard.onClick.AddListener(Leaderboard);

        btn_NoAds.onClick.RemoveAllListeners();
        btn_NoAds.onClick.AddListener(NoAds);
    }

    public void Play()
    {
        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.CarSelection);
        Destroy(this.gameObject);

    }

    public void RateUs()
    {

    }
    public void MoreGames()
    {

    }
    public void Leaderboard()
    {

    }
    public void FreeGift()
    {

    }
    public void RestorePurchase()
    {

    }
    public void NoAds()
    {

    }

}
