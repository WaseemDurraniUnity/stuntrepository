﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{
    public static string LeveltoLoad;
    public float loadlevelAfter = 2f;
    private void OnEnable()
    {
        // Invoke("LoadScene",loadlevelAfter);
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
      AsyncOperation asyn  = SceneManager.LoadSceneAsync(LeveltoLoad);
        while (!asyn.isDone)
        {
            yield return null;

        }

        if (MainMenuController.instance.headerObj)
            Destroy(MainMenuController.instance.headerObj);


 

        Destroy(this.gameObject);

    }


}
