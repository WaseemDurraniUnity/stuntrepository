﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSelectionCards : MonoBehaviour
{
    public delegate void Selected();
    public static Selected Setselected;
    public delegate void Unselected();
    public static Unselected SetOtherUnselected;
    public GameObject lockObj;
    public Image cardImage;
    public Image renderImage;
    public int carIndex;
    public GameObject selectedOBj;
    private Button carSelectButton;

    private void Awake()
    {
        carSelectButton = GetComponent<Button>();
        carSelectButton.onClick.AddListener(OnTap);
    }

    private void OnEnable()
    {
        Setselected += SetSelected;
        SetOtherUnselected += SetUnselected;

    }

    private void OnDisable()
    {
        Setselected -= SetSelected;
        SetOtherUnselected -= SetUnselected;
    }

    private void Update()
    {
        if (carIndex == MainMenuController.instance.GetCarIndex())
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1.05f, 1.05f, 1.05f), Time.deltaTime * 5f);
        if (carIndex != MainMenuController.instance.GetCarIndex())
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1f, 1f, 1f), Time.deltaTime * 5f);

    }

    public void SetLocked()
    {
        lockObj.SetActive(true);
        cardImage.color = Color.gray;
        renderImage.color = Color.gray;
    }

    public void SetUnlocked()
    {
        lockObj.SetActive(false);
        cardImage.color = Color.white;
        renderImage.color = Color.white;
    }

   
    public void SetSelected()
    {
        if (carIndex == MainMenuController.instance.GetCarIndex())
        {
            selectedOBj.SetActive(true);

            GameConstants.SetCurrentCar(MainMenuController.instance.GetCarIndex());
        }
    }

    public void SetUnselected()
    {
        if (carIndex != MainMenuController.instance.GetCarIndex())
            selectedOBj.SetActive(false);
    }


    public void OnTap()
    {
        if (MainMenuController.instance.GetCurCar())
            MainMenuController.instance.GetCurCar().gameObject.SetActive(false);

        MainMenuController.instance.SetCarIndex(carIndex);
        MainMenuController.instance.SetCar();
        Setselected();
        SetOtherUnselected();
    }

}
