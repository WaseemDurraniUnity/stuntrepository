﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AlertMenuScript : MonoBehaviour
{
    public static UnityAction yesEvent, noEvent;

    public RectTransform dialogTransfrom;
    public Text txt_AlertTitle, txt_YesBtn, txt_NoBtn;
    public Button btn_Yes, btn_No;

    public static bool isOneButton = false;
    public static string title, yesText, NoText;

    // Start is called before the first frame update
    void OnEnable()
    {
        dialogTransfrom.anchoredPosition = new Vector2(-1000, 0);
        txt_AlertTitle.text = title;
        txt_YesBtn.text = yesText;
        btn_No.gameObject.SetActive(!isOneButton);
        txt_NoBtn.text = NoText;
        title = NoText = yesText = "";
        btn_No.onClick.RemoveAllListeners();
        btn_No.onClick.AddListener(NoButton);
        btn_Yes.onClick.RemoveAllListeners();
        btn_Yes.onClick.AddListener(YesButton);

    }

    // Update is called once per frame
    void Update()
    {
        dialogTransfrom.anchoredPosition = Vector2.Lerp(dialogTransfrom.anchoredPosition, Vector2.zero, Time.deltaTime * 5f);
    }


    public void YesButton()
    {
        if (yesEvent != null)
            yesEvent.Invoke();

        yesEvent = null;

        gameObject.SetActive(false);


    }

    public void NoButton()
    {
        if (noEvent != null)
            noEvent.Invoke();

        noEvent = null;

        gameObject.SetActive(false);


    }
}
