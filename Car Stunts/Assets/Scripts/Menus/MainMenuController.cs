﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public enum MenuStates
    {
        MainMenu , CarSelection , LevelSelection,Header , AlertMenu , LoadingMenu , GamePopup
    }


    public string path_MenuPath;
    public Transform menusParent;
    public MenuStates curMenuState;
    public static MainMenuController instance;
    public Camera customizationCam;
    public RCC_Camera cameraPrefab;
    private RCC_Camera carCam;
    public CarModel[] carsPrefabs;
    public Transform spawnPosition;
    private List<GameObject> spawnedCars;
    private RCC_CarControllerV3 cur_Car;
    private CarModel cur_CarModel;
    private int curCarIndex = -1;
    public Vector3 camInitPos, camFinalPos;
    public GameObject headerObj;


    private void Awake()
    {
        DontDestroyOnLoad(this);
        instance = this;
        camInitPos = customizationCam.transform.position;
        camFinalPos = camInitPos;
        camInitPos.z = 5f;
        camFinalPos.y = 3f;
        customizationCam.transform.position = camFinalPos;
    }

    // Start is called before the first frame update
    void Start()
    {
        SpawnAndResetCars();
        ShowMenu(MenuStates.Header);
        ShowMainmenu();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (customizationCam)
            customizationCam.transform.position = Vector3.Lerp(customizationCam.transform.position, camInitPos, Time.deltaTime * 2f);
    }


    public void SetCarIndex(int carIndex)
    {
        curCarIndex = carIndex;
    }

    public int GetCarIndex()
    {
        return curCarIndex;
    }

    public RCC_CarControllerV3 GetCurCar()
    {
        return cur_Car;
    }

    public CarModel GetCurCarModel()
    {
        return cur_CarModel;
    }

    public void SpawnAndResetCars()
    {
        GameObject carParent = new GameObject("Cars");
        spawnedCars = new List<GameObject>();
        for (int i = 0;i<carsPrefabs.Length;i++)
        {
            GameObject spawnedCar = Instantiate(carsPrefabs[i].carPrefab,spawnPosition.position,spawnPosition.rotation,carParent.transform);
            RCC_CarControllerV3 carScript = spawnedCar.GetComponent<RCC_CarControllerV3>();
            carScript.canControl = false;
            carScript.KillEngine();
            spawnedCar.SetActive(false);
            spawnedCars.Add(spawnedCar);
        }

        SetNextCar();
    }


    public void SetNextCar()
    {
        if (cur_Car)
            cur_Car.gameObject.SetActive(false);
        curCarIndex = curCarIndex < spawnedCars.Count - 1 ? curCarIndex + 1 : 0;
        SetCar();

    }


    public void SetPreviousCar()
    {
        if (cur_Car)
            cur_Car.gameObject.SetActive(false);
        curCarIndex = curCarIndex > 0 ? curCarIndex - 1 : spawnedCars.Count - 1;
        SetCar();
    }

    public void SetCar()
    {
        // Setting Car
        cur_Car = spawnedCars[curCarIndex].GetComponent<RCC_CarControllerV3>();
        cur_CarModel = carsPrefabs[curCarIndex];
        cur_Car.gameObject.SetActive(true);
        if (CarSelectionCards.Setselected != null)
            CarSelectionCards.Setselected();
        if (CarSelectionCards.SetOtherUnselected != null)
            CarSelectionCards.SetOtherUnselected();
        if (CarSelectionScript.SetCarDetails != null)
            CarSelectionScript.SetCarDetails();
    }


    public void Customization()
    {
        customizationCam.gameObject.SetActive(false);
        carCam = Instantiate(cameraPrefab);
        // Setting Camera
        carCam.SetTarget(cur_Car.gameObject);
    }



    #region Menus

    public void ShowMainmenu()
    {
        ShowMenu(MenuStates.MainMenu);
    }

    public void ShowMenu(MenuStates state)
    {
        curMenuState = state;
        
        GameObject obj = Instantiate(Resources.Load<GameObject>(path_MenuPath + "" + curMenuState),menusParent);
        if (state == MenuStates.Header)
            headerObj = obj;
    }    
    



    #endregion


}
