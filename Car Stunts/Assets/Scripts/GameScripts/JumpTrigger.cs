﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpTrigger : MonoBehaviour
{
    RCC_CarControllerV3 carController;

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.carCamera.ChangeCamera(RCC_Camera.CameraMode.CINEMATIC);
            carController = other.gameObject.transform.root.gameObject.GetComponent<RCC_CarControllerV3>();
            Time.timeScale = .2f;
            Invoke("stopCinematic", 2.3f);
        }
    }

    void stopCinematic()
    {
        GameManager.instance.carCamera.ChangeCamera(RCC_Camera.CameraMode.TPS);
        Time.timeScale = 1f;


    }
}
