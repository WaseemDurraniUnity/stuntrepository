﻿using UnityEngine;

public class LevelEndTrigger : MonoBehaviour
{
    RCC_CarControllerV3 carController;
    private bool isSpawned;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //GameManager.instance.carCamera.ChangeCamera(RCC_Camera.CameraMode.CINEMATIC);
            //Time.timeScale = .2f;
            //carController = other.gameObject.transform.root.gameObject.GetComponent<RCC_CarControllerV3>();
            if (!isSpawned)
            {
                isSpawned = true;
                GameConstants.unlockItem(GameConstants.levelLockingKey + "" + (GameConstants.GetCurrentLevelToPlay() + 1));
                GameManager.instance._uiManager.HideControls();
                GamePopups.curState = GamePopups.PopupState.GameComplete;
                MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.GamePopup);
            }
        }
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        if (carController.canControl)
    //            Invoke("ResetTime", 2f);

    //        carController.canControl = false;
    //        carController.steerInput = -1;
    //        //carController.KillEngine();
    //    }
    //}


    //void ResetTime() 
    //{
    //    Time.timeScale = 1f;


    //}
}
