﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("CountDown")]
    public GameObject countDownObj;
    public Text txt_Countdown;
    [Header("Controls")]
    public GameObject controls;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Countdown
    public void ShowCountDown()
    {
        countDownObj.SetActive(true);

    }

    public void HideCountDown()
    {
        countDownObj.SetActive(false);
    }

    public void ShowCountDownText(string text)
    {
        txt_Countdown.gameObject.SetActive(true);
        txt_Countdown.text = text;
    }

    public void HideCountDownText()
    {
        txt_Countdown.gameObject.SetActive(false);
    }

    #endregion

    #region Controls

    public void ShowControls()
    {
        controls.SetActive(true);
    }

    public void HideControls()
    {
        controls.SetActive(false);

    }

    #endregion
}
