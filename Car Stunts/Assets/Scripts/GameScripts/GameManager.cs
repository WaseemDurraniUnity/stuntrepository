﻿using System.Collections;
using UnityEngine;
public class GameManager : MonoBehaviour
{
    //creating instance
    public static GameManager instance;

    [Header("Testing")]
    public bool isTesting = false;
    public int curCarIndex = 0;

    [Header("Player Car")]
    public GameObject[] allCars;
    private RCC_CarControllerV3 curCar;
    public RCC_Camera carCamera;
    public Transform carSpawnPosition;
  

    [Header("UI")]
    public UIManager _uiManager;

    [Header("Levels")]
    public GameObject[] levelsPrefabs;
    public int curLevel = 0;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        CreatLevel();

    }


  
    // Update is called once per frame
    void Update()
    {
        
    }

    #region levels System

    public void CreatLevel()
    {
        if (!isTesting)
            curLevel = GameConstants.GetCurrentLevelToPlay();

        GameObject level = Instantiate(levelsPrefabs[curLevel]);
        LevelPrefabScript levelsScript = level.GetComponent<LevelPrefabScript>();
        if (levelsScript != null)
        {
            carSpawnPosition = levelsScript.carSpawnPos;
            InitCarSettings();

        }
    }



    #endregion

    #region CarSettings

    public void InitCarSettings()
    {
        GameObject car = null;
        if (isTesting)
        {
            GameConstants.SetCurrentCar(curCarIndex);
            car = Instantiate(allCars[GameConstants.GetCurrentCar()], carSpawnPosition.position, carSpawnPosition.transform.rotation);
        }
        else
            car = Instantiate(MainMenuController.instance.carsPrefabs[GameConstants.GetCurrentCar()].carPrefab, carSpawnPosition.position, carSpawnPosition.transform.rotation);


        car.name = "PlayerCar";
        curCar = car.GetComponent<RCC_CarControllerV3>();
        curCar.KillEngine();
        carCamera.playerCar = curCar;
        //carCamera.ChangeCamera(RCC_Camera.CameraMode.CINEMATIC);
        carCamera.ChangeCamera(RCC_Camera.CameraMode.TPS);

        _uiManager.HideControls();

        GamePopups.curState = GamePopups.PopupState.Discription;
        GamePopups.discription = "Reach Finish Point As Fast You Can!";
        GamePopups.event_onCompleteDiscp = OnGameStart;
        MainMenuController.instance.ShowMenu(MainMenuController.MenuStates.GamePopup);
    }


    public void OnGameStart()
    {
        StartCoroutine(PlayStartCutScene());

    }

    IEnumerator PlayStartCutScene() 
    {
        _uiManager.ShowCountDown();
        for (int i = 1; i < 4; i++)
        {
            _uiManager.ShowCountDownText((4 - i).ToString());
            yield return new WaitForSeconds(1);
            _uiManager.HideCountDownText();

        }
        _uiManager.HideCountDown();
        _uiManager.ShowControls();


        RCC_AICarController curAiScript = curCar.GetComponent<RCC_AICarController>();
        if(curAiScript) Destroy(curAiScript);
        curCar.SetCanControl(true);
        curCar.KillOrStartEngine();
        //carCamera.ChangeCamera(RCC_Camera.CameraMode.TPS);
        

    }

    #endregion


}
