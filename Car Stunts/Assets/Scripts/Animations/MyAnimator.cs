﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyAnimator : MonoBehaviour
{
    public enum AnimationType
    {
        Position , Scale , Filler , FadeIn , FadeOut , FadeInOut
    }

    public enum PositionType
    {
        Left , Right , Top , Bottom
    }



    [Header("Animation Properities")]
    public AnimationType animationType;
    public float AnimationSpeed = 5f;
    public float startDelay = 0;
    public bool loop;
    //public bool AllowTextTypingAfter;
    private RectTransform myAnimationObject;
    private Vector3 destinationRect;
    private bool canAnimate;

    [Header("Position Properities")]
    public PositionType posType;
    private float maxAwayPos = 2000;

    [Header("Scaling Properities")]
    public float startingScale = 1.3f;

    [Header("Filling Properities")]
    public PositionType fillOrigin;
    private Image fillerImage;
    

    // Fader Properities
    private Color faderColor;
    private bool isFadein,Scaled;
    private Text FadeText;

    void OnEnable()
    {
        myAnimationObject = GetComponent<RectTransform>();
        if (myAnimationObject)
        {
            InitAnimation();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(myAnimationObject && canAnimate)
        {
            switch (animationType)
            {
                case AnimationType.Position:

                    myAnimationObject.anchoredPosition = Vector2.Lerp(myAnimationObject.anchoredPosition, destinationRect, Time.deltaTime * AnimationSpeed);

                    if (loop)
                    {
                        if(Approximately(myAnimationObject.anchoredPosition , destinationRect, 0))
                        {
                            InitAnimation();
                        }
                    }

                    break;

                case AnimationType.Scale:

                    myAnimationObject.localScale = Vector3.Lerp(myAnimationObject.localScale, destinationRect, Time.deltaTime * AnimationSpeed);

                    if (loop)
                    {
                        if (myAnimationObject.localScale.x >= 0)
                        {
                            InitAnimation();
                        }
                    }

                    break;


                case AnimationType.Filler:

                    if (!fillerImage)
                        return;

                    fillerImage.fillAmount = Mathf.Lerp(fillerImage.fillAmount, 1f, Time.deltaTime * AnimationSpeed);
       

                    break;

                case AnimationType.FadeIn:

                   

                    faderColor.a = Mathf.Lerp(faderColor.a, 1f, Time.deltaTime * AnimationSpeed);
                    if (fillerImage)
                        fillerImage.color = faderColor;
                    else if(FadeText)
                        FadeText.color = faderColor;


                    break;


                case AnimationType.FadeOut:

                    if (!fillerImage)
                        return;

                    faderColor.a = Mathf.Lerp(faderColor.a, 0f, Time.deltaTime * AnimationSpeed);
                    if (fillerImage)
                        fillerImage.color = faderColor;
                    else if (FadeText)
                        FadeText.color = faderColor;

                    break;

                case AnimationType.FadeInOut:

                    if (!fillerImage)
                        return;

                    if (isFadein)
                        faderColor.a = Mathf.Lerp(faderColor.a, 1f, Time.deltaTime * AnimationSpeed);
                    else
                        faderColor.a = Mathf.Lerp(faderColor.a, 0f, Time.deltaTime * AnimationSpeed);

                    if (fillerImage)
                        fillerImage.color = faderColor;
                    else if (FadeText)
                        FadeText.color = faderColor;

                    if (faderColor.a >= 0.99f)
                        isFadein = false;

                    break;
            }

        }
    }

    #region Animation Methods

    public void InitAnimation()
    {
        switch (animationType)
        {
            case AnimationType.Position:

                destinationRect = myAnimationObject.anchoredPosition;
                switch (posType)
                {
                    case PositionType.Top:

                        myAnimationObject.anchoredPosition = new Vector2(myAnimationObject.anchoredPosition.x,maxAwayPos);


                        break;


                    case PositionType.Bottom:

                        myAnimationObject.anchoredPosition = new Vector2(myAnimationObject.anchoredPosition.x, -maxAwayPos);

                        break;



                    case PositionType.Left:
                        myAnimationObject.anchoredPosition = new Vector2(-maxAwayPos, myAnimationObject.anchoredPosition.y);


                        break;



                    case PositionType.Right:

                        myAnimationObject.anchoredPosition = new Vector2(maxAwayPos, myAnimationObject.anchoredPosition.y);



                        break;

                }

                // End of position Nested Switch



                break;


            case AnimationType.Scale:

                destinationRect = myAnimationObject.localScale;

                myAnimationObject.localScale = new Vector3(startingScale, startingScale, startingScale);

                break;


            case AnimationType.Filler:

                fillerImage = GetComponent<Image>();
                fillerImage.type = Image.Type.Filled;
                fillerImage.fillAmount = 0;     
                switch (fillOrigin)
                {
                    case PositionType.Top:

                        fillerImage.fillMethod = Image.FillMethod.Vertical;
                        fillerImage.fillOrigin = 1;

                        break;


                    case PositionType.Bottom:

                        fillerImage.fillMethod = Image.FillMethod.Vertical;
                        fillerImage.fillOrigin = 0;

                        break;


                    case PositionType.Left:

                        fillerImage.fillMethod = Image.FillMethod.Horizontal;
                        fillerImage.fillOrigin = 0;

                        break;


                    case PositionType.Right:

                        fillerImage.fillMethod = Image.FillMethod.Horizontal;
                        fillerImage.fillOrigin = 1;

                        break;
                }



                // end of Image type nested Switch

                break;

            case AnimationType.FadeIn:

                if (GetComponent<Image>())
                {
                    fillerImage = GetComponent<Image>();
                    fillerImage.type = Image.Type.Simple;
                    faderColor = fillerImage.color;
                    faderColor.a = 0f;
                    fillerImage.color = faderColor;
                }

                else if (GetComponent<Text>())
                {
                    FadeText = GetComponent<Text>();
                    faderColor = FadeText.color;
                    faderColor.a = 0f;
                    FadeText.color = faderColor;

                }

                break;

            case AnimationType.FadeOut:
                if (GetComponent<Image>())
                {
                    fillerImage = GetComponent<Image>();
                    fillerImage.type = Image.Type.Simple;
                    faderColor = fillerImage.color;
                    faderColor.a = 1f;
                    fillerImage.color = faderColor;
                }

                else if (GetComponent<Text>())
                {
                    FadeText = GetComponent<Text>();
                    faderColor = FadeText.color;
                    faderColor.a = 1f;
                    FadeText.color = faderColor;

                }

                break;

            case AnimationType.FadeInOut:

                if (GetComponent<Image>())
                {
                    fillerImage = GetComponent<Image>();
                    fillerImage.type = Image.Type.Simple;
                    faderColor = fillerImage.color;
                    faderColor.a = 0f;
                    fillerImage.color = faderColor;
                }

                else if (GetComponent<Text>())
                {
                    FadeText = GetComponent<Text>();
                    faderColor = FadeText.color;
                    faderColor.a = 0f;
                    FadeText.color = faderColor;

                }
                isFadein = true;

                break;

        }


        // now wait if there is any delay in the start or start the animating
        StartCoroutine(WaitToAnimate());


    }

    IEnumerator WaitToAnimate()
    {
        canAnimate = false;
        yield return new WaitForSeconds(startDelay);
        canAnimate = true;
    }

    public bool Approximately(Vector3 me, Vector3 other, float allowedDifference)
    {
        var dx = me.x - other.x;
        if (Mathf.Abs(dx) > allowedDifference)
            return false;

        var dy = me.y - other.y;
        if (Mathf.Abs(dy) > allowedDifference)
            return false;

        var dz = me.z - other.z;

        return Mathf.Abs(dz) >= allowedDifference;
    }


    //private IEnumerator TextTyping()
    //{

    //}





    #endregion

}
