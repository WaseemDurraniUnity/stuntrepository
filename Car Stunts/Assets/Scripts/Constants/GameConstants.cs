﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstants : MonoBehaviour
{
    public static string carLockingKey = "Car_";
    public static string levelLockingKey = "Level_";
    public static string levelPlayKey = "LevelToPlay_";
    public static string starsKey = "Stars_";


    public static void SetCurrentCar(int carNumber)
    {
        PlayerPrefs.SetInt("CurrentCar", carNumber);
    }

    public static int GetCurrentCar()
    {
        return PlayerPrefs.GetInt("CurrentCar", 0);
    }

    public static bool isItemUnlocked(string id)
    {
        return PlayerPrefs.GetInt(id, 0) == 1 ? true : false;
    }

    public static void unlockItem(string id)
    {
        PlayerPrefs.SetInt(id, 1);
        PlayerPrefs.Save();

    }

    public static void AddCash(int val)
    {
        PlayerPrefs.SetInt("Cash", (GetCash() + val));
        PlayerPrefs.Save();
    }

    public static void DeductCash(int val)
    {
        PlayerPrefs.SetInt("Cash", (GetCash() - val));
        PlayerPrefs.Save();
    }

    public static int GetCash()
    {
        return PlayerPrefs.GetInt("Cash", 100);
    }


    public static void SetGold(int val)
    {
        PlayerPrefs.SetInt("Gold", (GetGold() + val));
        PlayerPrefs.Save();
    }

    public static void DeductGold(int val)
    {
        PlayerPrefs.SetInt("Gold", (GetGold() - val));
        PlayerPrefs.Save();
    }

    public static int GetGold()
    {
        return PlayerPrefs.GetInt("Gold", 50);
    }



    #region Levels

    public static void SetCurrentLevelToPlay(int levelno)
    {
        PlayerPrefs.SetInt(levelPlayKey, levelno);
        PlayerPrefs.Save();
    }

    public static int GetCurrentLevelToPlay()
    {
        return PlayerPrefs.GetInt(levelPlayKey);
    }


    public static void SetStars(int stars)
    {
        PlayerPrefs.SetInt(starsKey,stars);
        PlayerPrefs.Save();
    }

    public static int GetStars()
    {
        return PlayerPrefs.GetInt(starsKey, 0);
    }

    #endregion


}
