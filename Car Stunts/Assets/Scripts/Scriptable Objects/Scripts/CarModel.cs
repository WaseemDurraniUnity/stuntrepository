﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "CarModel", menuName = "Car Stunts/Cars", order = 1)]
public class CarModel : ScriptableObject
{
    public GameObject carPrefab;
    public string carName;
    public Sprite carRender;
    public float Speed;
    public float Power;
    public float Brake;
    public float Drift;
    public int priceinCash;
    public int priceinGold;

}
